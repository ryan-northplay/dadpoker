// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";
import Lobby from "../Lobby";


const {ccclass, property} = cc._decorator;

@ccclass
export default class SetBetRes extends NotifyComponent {
    
    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
     
           
        this.regist();
       
        
    }
 
   

    
    notify(data) {
        console.log("SetBetRes", data);
        // var call = cc.find('pageview').getComponent('pageView');
        // for(var a= 0 ; a < Lobby.betRange.length; a++){
        //     if(Lobby.betRange[a].betIndex == Lobby.betIndex){
        //         call.initPage(Lobby.betRange[a].maxMachineNum);
        //     }
            
        // }
        
        if(data.result){
            this.sendToServer(core.TYPE.PK5GAME, { 
                key: 'GetMachineList', 
                objKey: 104,
                data: {
                    
                }
            });
            
        }
        // pageView.initPage();
        
    
    }
        
  

    // update (dt) {}
}
