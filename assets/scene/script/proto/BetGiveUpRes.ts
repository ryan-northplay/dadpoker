// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";


const {ccclass, property} = cc._decorator;

@ccclass
export default class BetGiveUpRes extends NotifyComponent {
    
    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
        
    }
 
    start () {
        this.regist();
    }

    
    notify(data) {
        console.log("BetGiveUpRes", data);
    
    }
        
  

    // update (dt) {}
}
