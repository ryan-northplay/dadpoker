// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BetDoubleRes extends NotifyComponent {
    
    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
        
    }
 
    start () {
        this.regist();
    }

    
    notify(data) {
        console.log("BetDoubleRes", data);

        this.node.getChildByName("smallGame").getComponent("SmallGame").small_Game_Result_card(data.winResult.winPoker);

        this.node.getComponent("Game").refreshGold(data.nowGold);
        if(data.winResult.win){
            this.node.getComponent("Game").smallGameStart();
        }else{
            this.node.getComponent("Game").smallGameEnd();
            
            
           // this.node.getChildByName("smallGame").getComponent("SmallGame").smallGameOver();
           // this.node.getChildByName("elements").getComponent("AllGames").nextRound();
           // this.node.getComponent("Game").nextRound();
        }
    }
        
  

    // update (dt) {}
}
