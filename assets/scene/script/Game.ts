import {NT} from "../constant/constant";
import {core} from "../Core/Core";
import {NotifyComponent} from "../Core/view/NotifyComponent";
import Bet from "./Bet";
import BetDoubleRes from "./proto/BetDoubleRes";
import BetGiveUpRes from "./proto/BetGiveUpRes";
import SmallGame from "./SmallGame";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Game extends NotifyComponent {
    
    
    @property(cc.Component)
    labelarr: cc.Component[] = [];

    @property(cc.Component)
    elements: cc.Component[] = [];
    
    @property(cc.Button)
    allBetBtn: cc.Button = null;

    @property(cc.Button)
    BetBtn: cc.Button = null;

    @property(cc.Button)
    openBtn: cc.Button = null;

    @property(cc.Label)
    welLabel: cc.Label = null;

    @property(cc.Label)
    InsertLabel: cc.Label = null;

    @property(cc.Label)
    moneyLabel: cc.Label= null;

    @property(cc.Label)
    luckyTakeLabel: cc.Label = null;

    @property(cc.Button)
    smallGameBtn: cc.Button [] = [];

    @property(cc.Component)
    smallAnnouncement: cc.Component = null;

    @property(cc.Component)
    betArr: cc.Component [] = [];

    @property(cc.Component)
    jpArr: cc.Component [] = [];

    @property(cc.Label)
    jpListArr: cc.Label [] = [];

    @property(cc.Component)
    winarr : cc.Component = null;

    @property(cc.Label)
    huluStr: cc.Label = null;

    public static hulustr = 1;
    public static beforeGameStart = false;

    private WelLabelInterval = null;

    private smallGameCount = 0;

    private SmallGameBetNum = 0;


    onLoad() {
        Game.beforeGameStart = true;
        // Resources.instance.getSpriteFrame("gameSprite/item"+0, function (sp,path,key) {
        //     self.inviteBg.spriteFrame = sp;
        // },null,null,i);
        switch(NT.version){
            case "paman":
            this.node.getChildByName("elements").getComponent("AllGames").init_paman_Stage();
            
            NT.path = "prefab/paman/";
            NT.spriteName = "Paman";
            NT.spritePath = "gameSprite/paman/";
            NT.spriteNum = "num/black/black_";
        //    var scriptComponent = new cc.ComponentJS()
            //    this.addComponent(cc.)
                break;
            case "pacman":
                this.node.getChildByName("elements").getComponent("AllGames").init_pacman_Stage();
                NT.path = "prefab/pacman/";
                NT.spriteName = "SpriteBK";
                NT.spritePath = "gameSprite/pacman/";
                NT.spriteNum = "num/black/black_";
                break;

            case "apple":

                this.node.getChildByName("elements").getComponent("AllGames").init_apple_Stage();
                NT.path = "prefab/apple/";
                NT.spritePath = "gameSprite/apple/";
                NT.spriteName = "Apple";
                NT.spriteNum = "num/black/black_";
                break;
            case "sport":

                this.node.getChildByName("elements").getComponent("AllGames").init_sports_Stage();
                NT.path = "prefab/sport/";
                NT.spritePath = "gameSprite/sport/";
                NT.spriteName = "Sport";
                NT.spriteNum = "num/";
                break;
            case "poker":

                this.node.getChildByName("elements").getComponent("AllGames").init_poker_Stage();
                NT.path = "prefab/poker/";
                NT.spritePath = "gameSprite/poker/";
                NT.spriteName = "";
                NT.spriteNum = "";
                break;
        }
    }
 
    start () {
        this.regist();
        var blink  = cc.repeatForever(cc.blink(5, 5));
        this.labelarr[0].node.runAction(blink); 

      //  this.smallGameEnd();
        this.smallAnnouncement.node.active = false;
        this.refresh_JP();
     
    }

    refresh_JP(){
        for(var a = 0 ; a <　this.jpListArr.length; a++){
            if(a+1 < this.jpListArr.length-1){
                this.jpListArr[a].string = NT.jpList[a+1];
            }
            
        }
        this.jpListArr[4].string = NT.huluNum*10+"";
    }

    smallGameEnd(){
       

      
        var time = setTimeout(()=>{
            this.luckyTakeLabel.node.active = false;

            this.welLabel.node.runAction(
                cc.repeatForever(
                    cc.blink(1, 1)
            )
            );
            
            this.winarr.getComponent("CoinAnimate").runAnimate();
       ///     this.node.getChildByName("smallGame").getComponent("SmallGame").smallGameOver();
        //    this.node.getChildByName("elements").getComponent("AllGames").noWin();
            
            this.smallAnnouncement.node.active = false;

            
    
            for(var a = 0 ; a < this.jpArr.length; a++){
                this.jpArr[a].node.active = true;
            }
    
            for(var a = 0 ; a < this.betArr.length; a++){
                this.betArr[a].node.active = true;
            }
            
            clearTimeout(time);
        },1500);


    }

    smallGameStart(){
       // this.luckyTakeLabel.node.active = true;
       // this.luckyTakeLabel.string = "";
        this.smallAnnouncement.node.active = true;
      //  this.typeAni("LUCKY OR TAKE", this.luckyTakeLabel);
        this.welLabel.node.active = false;
        this.smallGameBtnsControl(true);
        

        for(var a = 0 ; a < this.jpArr.length; a++){
            this.jpArr[a].node.active = false;
        }

        for(var a = 0 ; a < this.betArr.length; a++){
            this.betArr[a].node.active = false;
        }

    }

    //打字機特效
  
    typeAni(text, label){
        var str = text ;
        var j = 0;
        label.string = "";
        this.schedule(()=>{
            label.string += str[j];
            j++;
            if(j == str.length-1){
                label.node.runAction(
                    cc.repeatForever(
                        cc.blink(1, 1)
                )
            );
            }
        }, 0.3, str.length-1, 0.3);
        
       
    }
    

    
    startGame(){
        this.initStage();
        
        if(Bet.round == 0 ){
         //   this.welLabel.node.stopAllActions();
         //   this.typeAni("WELCOME", this.welLabel);
        }

        if(Bet.round == 3){
          //  this.InsertLabel.node.active = false;
        }
        
        //一般開牌流程
        if(Bet.bet_Num[Bet.round] > 0){
            this.BetBtn.enabled = false;
            this.allBetBtn.enabled = false;
            this.openBtn.enabled = false;
            this.BetBtn.node.color = new cc.Color(135, 140, 140);
            this.allBetBtn.node.color = new cc.Color(135, 140, 140);
            this.openBtn.node.color = new cc.Color(135, 140, 140);
            this.node.getChildByName("elements").getComponent("AllGames").ShowhideBlinkName(false);
            this.openCard(Bet.round);
            
        }else{

            //棄牌流程 (第一個有壓住 後續沒壓住)
            if(Bet.round != 0){
                this.giveupCard();
               // this.node.getChildByName("elements").getComponent("AllGames").nextRound();

            }
            
            
        }
        
    }

    nextRound(){
       // this.InsertLabel.node.active = true;

    }

  

    backToLobby(){
        cc.director.loadScene("Lobby");
    }
    // start_Game(){
    //     if(Bet.bet_Num[Bet.round] > 0){
    //         this.gameName.node.active = false;
    //     }
        
    // }
    giveupCard(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'BetGiveUp', 
            objKey: 114,
            data: {
            }
        });
    }
    initStage(){
        for(var a= 0 ; a <　this.labelarr.length; a ++){
            this.labelarr[a].node.active =  false;
        }
    }

    compareBet(event, customEventData){

        if(Game.beforeGameStart){
            switch(parseInt(customEventData)){
                case 1:
                    Game.hulustr--;
                    if(Game.hulustr <= 0){
                        Game.hulustr = 13;
                    }
                    this.huluStr.string = "F-"+Game.hulustr;
                    break;
                    break;
                case 3:
                    Game.hulustr++;
                    if(Game.hulustr >= 13){
                        Game.hulustr = 1;
                    }
                    this.huluStr.string = "F-"+Game.hulustr;
                   

                default:
                break;
            }


        }else{

        
        if(this.smallGameCount <= 0){
            var data = this.node.getChildByName("elements").getComponent("AllGames").getMoneyReturnData();
            
            switch(parseInt(customEventData)){
            case 1:
                this.SmallGameBetNum = data.winResult.winGold * 2;
            break;

            case 2:
                this.SmallGameBetNum = data.winResult.winGold;
            break;

            case 3:
                this.SmallGameBetNum = data.winResult.winGold /2;
            break;

            }
            
            this.node.getChildByName("elements").getComponent("AllGames").smallGameCoinAnimate(data.winResult.winType, this.SmallGameBetNum);
            
           
            //多加一個jp有沒有中的判斷
            
            this.node.getChildByName("elements").getComponent("AllGames").smallGameStart();
            switch(NT.version){
                case "poker":
                    this.node.getChildByName("smallGame").getComponent("SmallGame").cards_for_poker(data.DoubleBetList);
                break;
                default:
                    this.node.getChildByName("smallGame").getComponent("SmallGame").cards_for_otherGame(data.DoubleBetList);
                break;
            }
         //   this.smallGameBtn[0].enabled = false;
           // this.node.getChildByName("smallGame").getComponent("SmallGame").init_Small_Game();
            this.smallGameCount++;
            this.welLabel.node.stopAllActions();
            this.welLabel.node.active = false;

        }else{
            
            var big = 0;
            var small = 0;
            switch(parseInt(customEventData)){
                case 1:
                    big = 1;
                    small = 0;
                break;
    
                case 3:
                    big = 0;
                    small = 1;
                break;
        }
        this.doubleClickSmallGame(big , small);
        this.smallGameCount = 0;
        this.smallGameBtnsControl(false);
    }
        

    }
}

    smallGameBtnsControl(status){
        for(var a = 0; a <this.smallGameBtn.length; a++){
            this.smallGameBtn[a].enabled = status;
        }

    }
    
    doubleClickSmallGame(big, small){

        console.log("sendMessage");

        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'BetDouble', 
            objKey: 112,
            data: {
                betBigger: big,
                betSmaller: small,
                betNum: this.SmallGameBetNum
            }
        });
    }

    openCard(count){

      //  if(Bet.bet_Num != 0){
       
            this.sendToServer(core.TYPE.PK5GAME, { 
                key: 'Bet', 
                objKey: 110,
                data: {
                    betNum: Bet.bet_Num[count],
                    f: Game.hulustr
                }
            });
      //  }
       
    }

  

    giveUpBet(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'BetGiveUp', 
            objKey: 114,
            data: {
               
            }
        });
    }

    leaveRoom(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'LeaveMachine', 
            objKey: 116,
            data: {
               
            }
        });
    }

    //刷新金額
    refreshGold(money){
        NT.money = money;
        this.moneyLabel.string = NT.money+"";
    }


    
    notify(data) {
        console.log("betResponse", data);
        this.BetBtn.enabled = true;
        this.allBetBtn.enabled = true;
        this.openBtn.enabled = true;
        this.BetBtn.node.color = new cc.Color(255, 255, 255);
        this.allBetBtn.node.color = new cc.Color(255,255,255);
        this.openBtn.node.color = new cc.Color(255,255,255);
        
        Bet.round++;

        Game.beforeGameStart = false;
        this.smallGameBtnsControl(false);
        
        this.node.getChildByName("elements").getComponent("AllGames").generator_Card(data);
        if(data.pockers.length >= 7){
            this.refreshGold(data.nowGold);
        }
        NT.jpList =  data.jpList;
        
        this.refresh_JP();
     //   this.refreshGold(data.nowGold);
          
      
    
    
       
    }
        
  

    // update (dt) {}
}
