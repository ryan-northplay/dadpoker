// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NT} from "../constant/constant";
import {core} from "../Core/Core";
import {NotifyComponent} from "../Core/view/NotifyComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class editboxChange extends cc.Component {

    @property(cc.EditBox)
    editbox : cc.EditBox = null;

    @property(cc.Label)
    id: cc.Label = null;

    @property(cc.Label)
    pw: cc.Label = null;

    @property(cc.Button)
    loginBtn: cc.Button = null;

    private canvasWidth: number = 0;
    private canvasHeight: number = 0;

    
    
 
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad(): void {
        this.canvasWidth = cc.game.canvas.width;
        this.canvasHeight = cc.game.canvas.height;
        window.addEventListener("resize", this.onResize);
        window.addEventListener("orientationchange", this.onResize);

     
    }
 
    onDestroy(): void {
        window.removeEventListener("resize", this.onResize);
        window.removeEventListener("orientationchange", this.onResize);
    }
 
    private onResize = (e: Event): void => {
        if (this.canvasWidth === cc.game.canvas.width 
            && this.canvasHeight === cc.game.canvas.height) { return; }
        this.resetSize();
    }
 
    public resetSize = (): void => {
        this.canvasWidth = cc.game.canvas.width;
        this.canvasHeight = cc.game.canvas.height;
        let editBox: any = this.node.getComponent(cc.EditBox);
        if (editBox == null) { return; }
        let isFocused: any = editBox.isFocused();
        if (editBox._impl) {
            editBox._impl.clear();
        } else {
            return;
        }
        let EditBoxClass: any = cc.EditBox;
        let impl: any = editBox._impl = new EditBoxClass._ImplClass();
        impl.init(editBox);
        editBox._updateString(editBox._string);
        editBox._syncSize();
        if (isFocused) { editBox.focus(); }
    }

    onEditDidBegan (editbox, customEventData) { 
        
       
        
    }
        //假設這個回調是給 editingDidEnded 事件的 
    onEditDidEnded (editbox, customEventData) { 
        //這裡 editbox 是一個 cc.EditBox 對象 
        //這裡的 customEventData 參數就等於你之前設置的 "foobar" 
        // console.log(customEventData);
        // console.log(this.id.string); 
        // console.log(this.pw.string); 
        //假設這個回調是給 textChanged 事件的 
    }
    onTextChanged (text, editbox, customEventData) { 
        //這裡的 text 表示 修改完後的 EditBox 的文本內容 
        //這裡 editbox 是一個 cc.EditBox 對象 //這裡的 customEventData 參數就等於你之前設置的 "foobar" 
       
    }
        //假設這個回調是給 editingReturn 事件的 
    onEditingReturn (editbox, customEventData) { 
            //這裡 editbox 是一個 cc.EditBox 對象 
            //這裡的 customEventData 參數就等於你之前設置的 "foobar" 
       

    }

       
    checkInfoExist(){
        if(this.id.string.length > 0 && this.pw.string.length > 0){
           
            this.node.parent.getComponent("loginRequest").sendInfo();
        }else{
            //帳號或密碼有缺失

            cc.resources.load("prefab/base", (errorMessage,loadedResource)=>{
                //检查资源加载
                if( errorMessage ) { cc.log( '載入資源失敗, 原因:' + errorMessage ); return; }
                if( !( loadedResource instanceof cc.Prefab ) ) { cc.log( '你载入的不是预制资源!' ); return; } 
                //开始实例化预制资源
                var TipBoxPrefab = cc.instantiate(loadedResource);
                TipBoxPrefab.setPosition(cc.game.canvas.width/2, cc.game.canvas.height/2);
                this.node.setPosition(this.node.parent.getPosition().x/2, this.node.parent.getPosition().y);
                //将预制资源添加到父节点
                 this.node.parent.addChild(TipBoxPrefab);
                 
                //获取预制资源中的js组件，并作出相应操作
                var TipBoxScript = TipBoxPrefab.getComponent('prefabScript');
                TipBoxScript.showWindow();
                //开始操作JS组件脚本
                TipBoxScript.message("message"); //设置提示框的内容
              
           });
        }

    }

   

    
   
        
  

    // update (dt) {}
}
