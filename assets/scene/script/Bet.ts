

import {NT} from "../constant/constant";
import {core} from "../Core/Core";
import {NotifyComponent} from "../Core/view/NotifyComponent";
import Lobby from "./Lobby";
import lobby from "./Lobby";
import BetGiveUpRes from "./proto/BetGiveUpRes";



const { ccclass, property } = cc._decorator;
@ccclass
export default class Bet extends cc.Component {
    /**数据 */
    
    // private defaultValue = lobby.miniBetMoney[0];
    private count : number = 0;
    
    @property(cc.Label)
    num: cc.Label[] = [];

    @property(cc.Label)
    point: cc.Label;

    public static bet_Num: number[] =[0,0,0,0] ;
    public static round: number = 0;
    
    private max: number = 50;

    private money = 0;
    onLoad() {
           
        
    }

    start () {
        

        this.point.string = NT.money;
        this.money = NT.money;

        for(var a =0 ; a < this.num.length; a++){
            this.num[a].string = "0";
            Bet.bet_Num[a] = 0;
        }
        this.count = 0;
        

    }
    nextRound(){
       
        for(var a =0 ; a < this.num.length; a++){
            this.num[a].string = "0";
            Bet.bet_Num[a] = 0;
        }
        this.count = 0;
    }

    allBet(){

        if(Bet.bet_Num[Bet.round] <= 0){
            if(Bet.bet_Num[Bet.round] == 0){
                Bet.bet_Num[Bet.round] = lobby.miniBetMoney[lobby.miniBetMoney.length-1];
                this.num[Bet.round].string = Bet.bet_Num[Bet.round] + "";
            }else{
                Bet.round++;
            }
    
            this.node.parent.getComponent("Game").startGame();
            this.money = this.money- Bet.bet_Num[Bet.round];
            NT.huluNum = NT.huluNum+ Bet.bet_Num[Bet.round];
            this.point.string = this.money+"";

        }
        
        
      
        


    }

    initStage(){
        

    }

    betPlus(){
    //800 是葫蘆
    //1000 鐵支
    //2000 順子
    //4000 五支 (四張+鬼牌)
    //10000 同花大排
    //console.log(lobby.miniBetMoney);
    if(Bet.round != 0 ){
        var count = lobby.miniBetMoney[2] - lobby.miniBetMoney[1];
       

        if(Bet.bet_Num[Bet.round] < lobby.miniBetMoney[lobby.miniBetMoney.length-1]){
            if(Bet.bet_Num[Bet.round] == 0){
                Bet.bet_Num[Bet.round] = lobby.miniBetMoney[0];
            }else{
                if(Bet.bet_Num[Bet.round] >= Bet.bet_Num[Bet.round-1]){
                    Bet.bet_Num[Bet.round] = Bet.bet_Num[Bet.round-1];
                    
                }else{
                    Bet.bet_Num[Bet.round] = Bet.bet_Num[Bet.round]+count;
                   
                }
                
                
               
                
            }
            this.num[Bet.round].string = Bet.bet_Num[Bet.round] + "";
          //  Bet.round = this.count;
            //NT.money = NT.money - Bet.bet_Num[this.count];
            this.money = this.money - Bet.bet_Num[Bet.round];
            this.point.string = this.money+""; 
            NT.huluNum = NT.huluNum+ Bet.bet_Num[Bet.round];
            if(Bet.bet_Num[Bet.round] == lobby.miniBetMoney[lobby.miniBetMoney.length-1]){
               
                this.node.parent.getComponent("Game").startGame();
            }

        }else{
           
            if(Bet.round < lobby.miniBetMoney.length){
                Bet.round ++;
                this.betPlus();  
            }
           
        }

        
    }

   

       
       
    
    }


  


}
