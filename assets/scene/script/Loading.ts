import {NotifyComponent} from "../Core/view/NotifyComponent";
import {core} from "../Core/Core";
import {NT} from "../constant/constant";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Loading extends cc.Component{


    @property(cc.Label)
    prolabel: cc.Label = null;

    @property(cc.ProgressBar)
    progress: cc.ProgressBar ;

    public resolve ;
    public reject;
    private folderName = "";

    start () {
       // cc.resources.loadDir("image"  , this.onprocess.bind(this), this.oncomplete.bind(this));
       try{
        new Promise((resolve, reject)=>{
            cc.loader.loadResDir("image"  , this.onprocess.bind(this), this.oncomplete.bind(this));
            this.resolve = resolve;
            this.reject = reject;
            this.folderName = "圖檔";
        }).then((value)=>{
            this.folderName = "音檔";
            cc.loader.loadResDir("sound"  , this.onprocess.bind(this), this.oncomplete.bind(this));  
        }).catch((e)=>{
            console.log(e);
        })
       }catch(e){
         this.reject("fail");
       }
    }
    onprocess(completeCount, totalCount, item){
        var persent = (completeCount)/totalCount;
        this.prolabel.string = "加載"+this.folderName+"..."+(persent*100).toFixed(0)+"%";
        this.progress.progress = persent;    
    }
    oncomplete (err , spriteFrame){
        if(err){
            this.reject("fail");
        }else{
            this.resolve("OK");
            if(this.folderName == "音檔"){
                cc.director.loadScene("Lobby");
            }
        }
    }
}
