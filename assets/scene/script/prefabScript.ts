// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NT} from "../constant/constant";
import {core} from "../Core/Core";
import {NotifyComponent} from "../Core/view/NotifyComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class prefabScript extends cc.Component {
    
    @property(cc.Component)
    window: cc.Component = null;

    @property(cc.Button)
    btn: cc.Button = null;

    @property(cc.Label)
    str : cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
        this.window.node.active = false;
    //    this.regist();
        
    }
    start () {
      
    }

    showWindow(){
        this.window.node.active = true;
    }

    message(text){
        this.str.string = text;
    }

    closeWindow(){
        this.window.node.active = false;
    }
  

  

 
        
  

    // update (dt) {}
}
