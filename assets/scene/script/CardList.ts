// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NT} from "../constant/constant";
import {core} from "../Core/Core";
import {NotifyComponent} from "../Core/view/NotifyComponent";
import AllGames from "./AllGames";
import Game from "./Game";
import {GameSet} from "../script/GameTypeSet";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CardList extends cc.Component {

    
    private flower = 0;
    private cardNum = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
        
    //    this.regist();
       
        
    }
    calculatePokerFlower(num){
        console.log("=============="+num);
        var name = "";
        switch(Math.floor(num/13)){
            case GameSet.CardState.Spade:
                //黑桃
                //數字部分 num % 13
                var calcu = num%13 +1;
                name = "t"+calcu;
               
                break;
            case GameSet.CardState.Hearts:
                //紅心
                var calcu = num%13 +1;
                name = "h"+calcu;
           

             
                break;
            case GameSet.CardState.Diamonds:
            //方塊
            var calcu = num%13 +1;
                name = "f"+calcu;
              

                
                break;

            case GameSet.CardState.Club:

                //梅花
                var calcu = num%13 +1;
                name = "m"+calcu;
               

                break;
            case GameSet.CardState.Ghost:

                //鬼牌
                name = "joker";
                
                

               
                break;
        }
        return NT.spritePath+name;

    }
    //52 53 鬼牌
    //根據server數字計算卡牌上花色
    calculateOtherGameFlower(num){
        console.log("=============="+num);
        switch(Math.floor(num/13)){
            case GameSet.CardState.Spade:
                //黑桃
                //數字部分 num % 13
                this.flower = 1;
                this.cardNum = num%13 +1;
                break;
            case GameSet.CardState.Hearts:
                //紅心
                this.flower = 2;
                this.cardNum = num%13 +1;

             
                break;
            case GameSet.CardState.Diamonds:
            //方塊
                
                this.flower = 3;
                this.cardNum= num%13 +1;

                
                break;

            case GameSet.CardState.Club:

                //梅花
                this.flower = 4;
                this.cardNum= num%13 +1;

                break;
            case GameSet.CardState.Ghost:

                //鬼牌
               // var number = num%13 +1;
               var path = "";
                if(num == 52 || num == "52"){
                   path =  NT.spritePath+NT.spriteName+"_06";
                }else if(num == 53 || num == "53"){
                    path = NT.spritePath+NT.spriteName+"_07";
                }
                
                return path;
                break;
        }

       return this.generatePath();

    }

    //根據server數字計算卡牌上數字多少
     getCardNum(num){
        var sportColor = "";
        switch(Math.floor(num/13)){
            case GameSet.CardState.Spade:
                //黑桃
                //數字部分 num % 13
                this.flower = 1;
                this.cardNum = num%13 +1;
                sportColor = "blue/blue_";

                break;

            case GameSet.CardState.Hearts:
                //方塊
                this.flower = 2;
                this.cardNum = num%13 +1;
                sportColor = "red/red_";
             
                break;
            case GameSet.CardState.Diamonds:

                //紅心
                this.flower = 3;
                this.cardNum= num%13 +1;
                sportColor = "red/red_";
                
                break;

            case GameSet.CardState.Club:

                //梅花
                this.flower = 4;
                this.cardNum= num%13 +1;
                sportColor = "blue/blue_";
               
                break;
            case GameSet.CardState.Ghost:

                //鬼牌
             //   var number = num%13 +1;

               
                break;
        }


        //如果數字小於十 數字圖名字
        switch(NT.version){
            case "sport":
                if(this.cardNum <　10){
                    return NT.spriteNum+sportColor+"0"+this.cardNum;
                }else{
                    return NT.spriteNum+sportColor+this.cardNum;
                }

            break;

            default:
                if(this.cardNum <　10){
                    return NT.spriteNum+"0"+this.cardNum;
                }else{
                    return NT.spriteNum+this.cardNum;
                }
            break;
        }

     

        
        
     }

     //生成花色的路徑
     generatePath(){
        var string = NT.spritePath+NT.spriteName+"_0"+this.flower;

        return string;
       

    }

    start () {
       


    }
  


  
    }
  


    
   
        
  



