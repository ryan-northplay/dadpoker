export class GameSet{
    public static CardState = {
        Spade:0,     //黑桃
        Hearts:1,    //红桃
        Diamonds:2,      //梅花
        Club:3,   //方块
        Ghost: 4    //鬼牌

    }

    public static WinState = {
        Nothing: 0, //沒中
        OnePair: 1, //對子
        TwoPair: 2, //兩隊
        ThreeOfKing: 3, //三條
        Straight: 4, //順子
        Flush: 5, //同花
        FullHouse: 6, //葫蘆
        FourOfKing:　7, //四條
        StraightFlush: 8, //同花順
        FourOfKingWithJoker: 9, //四條+鬼牌
        StraightFlushWithA: 10 //同花順+A
    }

}