import {NotifyComponent} from "../Core/view/NotifyComponent";
import {core} from "../Core/Core";
import {NT} from "../constant/constant";
import GetMachineListRes from "./proto/GetMachineListRes";
import { BaseClass } from "./BaseClass";
const {ccclass, property} = cc._decorator;

@ccclass
export default class Lobby extends NotifyComponent {
 

    @property(cc.Component)
    RoomBtn : cc.Component[]= [];

    @property(cc.Sprite)
    titleString: cc.Sprite[] = [];

    @property(cc.Button)
    backBtn: cc.Button = null;

    @property(cc.Component)
    jpList: cc.Component [] = [];

    @property(cc.Component)
    detailWin: cc.Component = null;

    private betRange = null;
    public static miniBetMoney = null;
    private machineNum = null;
    private jpArr = [];

     onLoad () {
        this.regist();
        this.detailWin.node.active = false;
     }

    start () {
        cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE);

        this.sendToServer(core.TYPE.PK5Client, { 
            key: 'GetBetRange', 
            objKey: 100,
            data: {
            }
        });
        
        this.changeRoomStatus(true);

         //设置排行榜数据
        //  let rankData = [];
        //  for(let i=0;i<100;i++){
        //      rankData.push({rank:i, name:"名称"});
        //  }
 
        //  this.rankList.setData(rankData);
    }

    openDetail(data){
        this.detailWin.node.active = true;
        for(var a = 0; a < data.jpList.length; a++){
            this.jpList[a].getComponent(cc.Label).string = BaseClass.arrayJPList[a] + data.jpList[a];
        }
        this.jpArr = data.jpList;
        this.machineNum = data.machindId;

    }

    intoGame(event, customEventData){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'GetMachineDetial', 
            objKey: 106,
            data: {
                machindId: event.target.parent.getChildByName("num")._renderComponent._string
            }
        });
    }

    closeDetailWin(){
        this.detailWin.node.active = false;
        this.machineNum = null;
    }

    enterGame(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'IntoGame', 
            objKey: 108,
            data: {
                machindId: this.machineNum
            }
        });
        NT.jpList = this.jpArr;
    }

    Back_Room(){
      this.changeRoomStatus(true);
    }

    //區間房狀態
    changeRoomStatus(status){
        for(var a = 0; a < this.RoomBtn.length; a++){
            this.RoomBtn[a].node.active = status;
        }
        this.backBtn.node.active = !status;
    }

    Choose_Room(event, customEventData){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'SetBet', 
            objKey: 102,
            data: {
                betIndex: this.betRange.betranges[customEventData].betIndex
            }
        });
        Lobby.miniBetMoney =  this.betRange.betranges[customEventData].miniBetMoney; 
    }

    notify(data){
        console.log("Lobby",data);
        
        this.betRange = data;
        
        for(let a = 0 ; a < this.betRange.betranges.length; a++){
            var gen = this.getComponent("spriteAtlas").generate_Num(this.betRange.betranges[a].betName, this.titleString[a].node);
            const arr = this.betRange.betranges[a].betName.split("");
            if(arr.length > 5){
                gen.x= this.titleString[a].node.x-((arr.length-5)*17)-95;
            }else{
                gen.x= this.titleString[a].node.x-95;
            }
            
            gen.y = this.titleString[a].node.y-200;
            //this.titleString[a].string = this.betRange.betranges[a].betName;

        }
           
        
       
    }
    // update (dt) {}
}
