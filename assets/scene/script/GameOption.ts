// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {core} from "../Core/Core";
import {NT} from "../constant/constant";
const {ccclass, property} = cc._decorator;

@ccclass
export default class GameOption extends cc.Component {

    @property(cc.Component)
    window: cc.Component = null;

    @property(cc.Component)
    square: cc.Component = null;

    @property(cc.Button)
    btnArr: cc.Button[] = [];


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.window.node.active = false;
        NT.version = "apple";
        
    }

    showWindow(){
        if(this.window.node.active){
            this.window.node.active = false;
        }else{
            this.window.node.active = true;
            for(var a= 0 ; a < this.btnArr.length; a++){
                this.btnArr[a].node.on(cc.Node.EventType.TOUCH_START, this.chooseFunction, this);
                if(this.btnArr[a].node.name == NT.version){
                    this.square.node.position = this.btnArr[a].node.position;
                }
            }
            
            
        }
    }

    chooseFunction(eventTarget){
        
        this.square.node.active = true;
        this.square.node.position = eventTarget.target.position;
        NT.version = eventTarget.target.name;
        
    }

    closeWindow(){
        this.window.node.active = false;
    }

    // update (dt) {}
}
