// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


const {ccclass, property} = cc._decorator;

@ccclass
export default class spriteAtlas extends cc.Component {
    
  
    @property(cc.SpriteAtlas)
    num : cc.SpriteAtlas = null;

   
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
        
    //    this.regist();
       
 
       
        
    }
    start () {
       
        this.num.getSpriteFrame("1");
    }
  

    generate_Num(name, newNode){
        const arr = name.split("");
        
        var b = new cc.Node();
        for(let a = 0 ; a <arr.length; a++){
            var n = new cc.Node();
            n.addComponent(cc.Sprite);
            if(arr[a] == "-"){
                n.getComponent(cc.Sprite).spriteFrame = this.num.getSpriteFrame("10");
            }else{
                n.getComponent(cc.Sprite).spriteFrame = this.num.getSpriteFrame(arr[a]);
            }
            n.parent = b
            n.x = a*43;
            n.y = 0;
         
            n.anchorX = 0.5;
            n.anchorY = 0.5;
        }
        b.parent = newNode;
        return b;
       

    }
  

    // update (dt) {}
}
