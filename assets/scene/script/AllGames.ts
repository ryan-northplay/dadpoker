// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Bet from "./Bet";
import CardList from "./CardList";
import {NT} from "../constant/constant";
import {GameSet} from "../script/GameTypeSet";
import Game from "./Game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AllGames extends cc.Component {

  
 
    // LIFE-CYCLE CALLBACKS:
    @property(cc.Component)
    elements: cc.Component[] = [];

    @property(cc.Component)
    title: cc.Component = null;

    @property(cc.Component)
    gameName: cc.Component = null;

    @property(cc.Component)
    coinAnimation: cc.Component = null;

    @property(cc.Component)
    betComponent: cc.Component = null;

    @property(cc.Component)
    GameCanvas: cc.Component = null;


    
    private count = 0;
    private background = null;
    private nodeNum = null;
    private cardNum = [];
    private spriteArr = [];
    private piccount = 0;
    private winMoney = null;
    

     onLoad () {}

    start () {
     
        //this.bigJPAnimation(1, 7);
        this.initPrefab();

    }

 
    bigJPAnimation(begin, end, data){
        this.coinAnimation.getComponent("CoinAnimate").bigJPAnimate(begin, end, data);
    }
  

    //讓array為空
    resetArray(){
        this.cardNum = [];
        this.spriteArr = [];
        this.piccount = 0;
    }

    //根據遊戲建立牌
    generator_Card(data){
        switch(NT.version){
            case "poker":
                this.cards_for_poker(data);
            break;

            default:
                this.cards_for_otherGame(data);
            break;
        }

    }





    //除了撲克牌以外的遊戲
    cards_for_otherGame(data){
        
        this.resetArray();

        for(var a = 0; a <　data.pockers.length; a++){
          
            this.cardNum.push(this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(data.pockers[this.piccount]));
            this.spriteArr.push(this.node.getComponents("CardList")[0].node.getComponent("CardList").calculateOtherGameFlower(data.pockers[this.piccount]))

            this.piccount++;
        }
        
        console.log(this.spriteArr);
         for(var a = 0; a < data.pockers.length;a++){
            if(!this.elements[a].node.active){
                this.elements[a].node.active = true;
            }

                 //如果牌是-1 給背牌
                if(data.pockers[a] == -1){
                    this.elements[a].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(NT.spritePath+NT.spriteName+"_05");
                    this.elements[a].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum("-1");
                }else{
                    if(data.pockers[a] == 52 || data.pockers[a] == "52" || data.pockers[a] == 53 || data.pockers[a] == "53"){
                        this.elements[a].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(this.spriteArr[a]);
                        this.elements[a].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum("ghost");
                    }else{
                        
                        this.elements[a].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(this.spriteArr[a]);
                        this.elements[a].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum(this.cardNum[a]);
                
                    }
                    
                }
         }

         if(this.elements.length == data.pockers.length){
            //獲勝
            if(data.result){
   
                // switch(data.winResult.winType){
                //     case GameSet.WinState.Nothing:
                //     case GameSet.WinState.OnePair:
                //     case GameSet.WinState.TwoPair:
                //     case GameSet.WinState.ThreeOfKing:
                //     case GameSet.WinState.Straight:
                //     case GameSet.WinState.Flush:
                //     case GameSet.WinState.FullHouse:
                //     case GameSet.WinState.FourOfKing:
                //     case GameSet.WinState.StraightFlush:
                //     case GameSet.WinState.FourOfKingWithJoker:
                //     case GameSet.WinState.StraightFlushWithA:
                // }
                if(data.winResult.winGold > 0){
                    if(data.winResult.winJP2){
                        this.win(data);
                        this.bigJPAnimation(data.winResult.winType, data.winResult.winJP2, data);
                    }else{
                        this.win(data);
                    }
                    
                }

                if(data.winResult.winPoker.length <= 0){

                    var time = setTimeout(()=>{

                        this.noWin();
                        clearTimeout(time);
                    });
                    
                }
                

            }
            
         }
    }

    win(data){
        this.coinAnimation.getComponent("CoinAnimate").coinAnimate(data.winResult.winType, data.winResult.winGold, 1);
        this.winMoney = data;
      //  this.GameCanvas.getComponent("Game").smallGameStart();
        

    }

    
    smallGameStart(){
        for(var a= 0 ; a < this.elements.length; a++){
            this.elements[a].node.active = false;
        }
    }
    smallGameCoinAnimate(winType, winGold){
        this.coinAnimation.getComponent("CoinAnimate").coinAnimate(winType, winGold, 2);

    }
    
    getMoneyReturnData(){
        return this.winMoney;
    }

    noWin(){
        this.nextRound();
        this.GameCanvas.getComponent("Game").nextRound();
    }
    cards_for_poker(data){
        this.resetArray();

        for(var a = 0; a <　data.pockers.length; a++){
            
            this.spriteArr.push(this.node.getComponents("CardList")[0].node.getComponent("CardList").calculatePokerFlower(data.pockers[this.piccount]));

            this.piccount++;
        }
        
         for(var a = 0; a < data.pockers.length;a++){
            if(!this.elements[a].node.active){
                this.elements[a].node.active = true;
            }
            
            //如果牌是-1 給背牌
            if(data.pockers[a] == -1){
                this.elements[a].node.getChildByName("f1").getComponent("PrefabSprite").spritePic(NT.spritePath+NT.spriteName+"pokerback");
                
            }else{
                this.elements[a].node.getChildByName("f1").getComponent("PrefabSprite").spritePic(this.spriteArr[a]);
            }
            
     
         }

         if(this.elements.length == data.pockers.length){
            //獲勝
            if(data.result){
   
                // switch(data.winResult.winType){
                //     case GameSet.WinState.Nothing:
                //     case GameSet.WinState.OnePair:
                //     case GameSet.WinState.TwoPair:
                //     case GameSet.WinState.ThreeOfKing:
                //     case GameSet.WinState.Straight:
                //     case GameSet.WinState.Flush:
                //     case GameSet.WinState.FullHouse:
                //     case GameSet.WinState.FourOfKing:
                //     case GameSet.WinState.StraightFlush:
                //     case GameSet.WinState.FourOfKingWithJoker:
                //     case GameSet.WinState.StraightFlushWithA:
                // }
                if(data.winResult.winGold > 0){
                    if(data.winResult.winJP2){
                        this.win(data);
                        this.bigJPAnimation(data.winResult.winType, data.winResult.winJP2, data);
                    }else{
                        this.win(data);
                    }
                }

                if(data.winResult.winPoker.length <= 0){
                    

                        this.noWin();
                   

                }
                
            }
            
         }


    }

    

    //下一局
    nextRound(){

        var time = setTimeout(()=>{
            for(var a= 0 ; a < this.elements.length; a++){
                this.elements[a].node.active = false;
            }
            this.ShowhideBlinkName(true);
            this.betComponent.getComponent("Bet").nextRound();
            this.count = 0;
            Bet.round = 0;
            Game.beforeGameStart = true;
            NT.huluNum = 0;
            this.GameCanvas.getComponent("Game").smallGameBtnsControl(true);
            this.GameCanvas.getComponent("Game").refresh_JP();

            clearTimeout(time);
        },1500);
       
    }


    //動態建立prefab
    initPrefab(){
        switch(NT.version){
            case "poker":
                this.initPokerGamePrefab();
            break;

            default:
                this.initOtherGamePrefab();
            break;
        }
    }

    


    initOtherGamePrefab(){
          
        for(var a = 0 ;a < 7; a++){
            cc.resources.load(NT.path+NT.spriteName+"_01",  (err, prefab) => {
                this.background = cc.instantiate(prefab);
             //   this.background.getComponent("PrefabSprite").loadResource(this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(data.pockers[a]));
                this.elements[this.count].node.addChild(this.background);
                
                this.elements[this.count].node.active = false;
                this.count ++;      
             });

        }
    }

    initPokerGamePrefab(){
        for(var a = 0 ;a < 7; a++){
            cc.resources.load(NT.path+"f1",  (err, prefab) => {
                this.background = cc.instantiate(prefab);
                
             //   this.background.getComponent("PrefabSprite").loadResource(this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(data.pockers[a]));
                this.elements[this.count].node.addChild(this.background);
                
                this.elements[this.count].node.active = false;
                this.count ++;      
            
             });


        }

    }

    
    init_paman_Stage(){
        var title =  this.getNewSprite("title", "gameSprite/paman/items/item_07" );
        title.parent = this.title.node;


        var gameName = this.getNewSprite("gameName", "gameSprite/paman/titleName" );
        gameName.parent = this.gameName.node;
        
        this.gameName.node.runAction(
            cc.repeatForever(
                cc.blink(1, 1)
            )
        );
        for(var a = 0; a < this.elements.length; a++){
            this.elements[a].node.scale = 0.8;
        }

        this.elements[0].node.setPosition(-950.147, -132.838);
        this.elements[1].node.setPosition(-622.434, -335.645);
        this.elements[2].node.setPosition(-298.75, -132.838);
        this.elements[3].node.setPosition(19.519, -335.645);
        this.elements[4].node.setPosition(342.712, -132.838);
        this.elements[5].node.setPosition(644.26, -335.645);
        this.elements[6].node.setPosition(977.701, -132.838);

    }
 

    init_pacman_Stage(){
        var title =  this.getNewSprite("title", "gameSprite/pacman/Sprite00" );
        title.parent = this.title.node;

        var gameName = this.getNewSprite("gameName", "image/TitleName00" );
        gameName.parent = this.gameName.node;
        
        // this.gameName.node.runAction(
        //     cc.repeatForever(
        //         cc.blink(1, 1)
        //     )
        // );
        for(var a = 0; a < this.elements.length; a++){
            this.elements[a].node.scale = 1.2;
        }
              
        this.elements[0].node.setPosition(-916.837, -239.311);
        this.elements[1].node.setPosition(-613.561, -286.845);
        this.elements[2].node.setPosition(-325.368, -239.311);
        this.elements[3].node.setPosition(-33.718, -286.845);
        this.elements[4].node.setPosition(267.294, -239.311);
        this.elements[5].node.setPosition(575.715, -286.845);
        this.elements[6].node.setPosition(880.101, -239.311);

    }

    init_sports_Stage(){
        var title =  this.getNewSprite("title", "gameSprite/sport/Sport00" );
        title.parent = this.title.node;

      for(var a = 0; a < this.elements.length; a++){
            this.elements[a].node.scale = 1.2;
        }

        var gameName = this.getNewSprite("gameName", "image/TitleName00" );
        gameName.parent = this.gameName.node;
        
        // this.gameName.node.runAction(
        //     cc.repeatForever(
        //         cc.blink(1, 1)
        //     )
        // );
              
        this.elements[0].node.setPosition(-916.837, -239.311);
        this.elements[1].node.setPosition(-613.561, -286.845);
        this.elements[2].node.setPosition(-325.368, -239.311);
        this.elements[3].node.setPosition(-33.718, -286.845);
        this.elements[4].node.setPosition(267.294, -239.311);
        this.elements[5].node.setPosition(575.715, -286.845);
        this.elements[6].node.setPosition(880.101, -239.311);

    }

    init_apple_Stage(){
        var title =  this.getNewSprite("title", "gameSprite/apple/Apple_00" );
        title.parent = this.title.node;

        var gameName = this.getNewSprite("gameName", "image/TitleName00" );
        gameName.parent = this.gameName.node;
        
        // this.gameName.node.runAction(
        //     cc.repeatForever(
        //         cc.blink(1, 1)
        //     )
        // );

        for(var a = 0; a < this.elements.length; a++){
            this.elements[a].node.scale = 0.7;
        }

        this.elements[0].node.setPosition(-950.147, -132.838);
        this.elements[1].node.setPosition(-622.434, -335.645);
        this.elements[2].node.setPosition(-298.75, -132.838);
        this.elements[3].node.setPosition(19.519, -335.645);
        this.elements[4].node.setPosition(342.712, -132.838);
        this.elements[5].node.setPosition(644.26, -335.645);
        this.elements[6].node.setPosition(977.701, -132.838);

    }
    init_poker_Stage(){
        // var title =  this.getNewSprite("title", "gameSprite/image/TitleName00" );
        // title.parent = this.title.node;

        var gameName = this.getNewSprite("gameName", "image/TitleName00" );
        gameName.parent = this.gameName.node;
        
        // this.gameName.node.runAction(
        //     cc.repeatForever(
        //         cc.blink(1, 1)
        //     )
        // );
  
        for(var a = 0; a < this.elements.length; a++){
            this.elements[a].node.scale = 1;
        }

        this.elements[0].node.setPosition(-872.473, -284.419);
        this.elements[1].node.setPosition(-582.506, -326.39);
        this.elements[2].node.setPosition(-289.877, -284.419);
        this.elements[3].node.setPosition(-2.663, -326.39);
        this.elements[4].node.setPosition(298.348, -284.419);
        this.elements[5].node.setPosition(589.024, -326.39);
        this.elements[6].node.setPosition(880.1, -284.419);

        
    }

    ShowhideBlinkName(control){
        this.gameName.node.active = control;
    }


    getNewSprite(nodeName : string ,url: string ):cc.Node {

        var node = new cc.Node(nodeName);
        var sprite = node.addComponent(cc.Sprite);
        var textures =[];
    
        cc.resources.load(url , cc.Texture2D, ( error, texture: cc.Texture2D ) => {
            // console.log(url,texture)
            if( !error ){
                var sf = new cc.SpriteFrame();
                sf.setTexture(texture);
                sprite.spriteFrame = sf;
    
            }
        });

        return node;

    }
    // update (dt) {}
}


