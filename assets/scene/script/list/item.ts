// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import prefabMachine from "./prefabMachine";


const {ccclass, property} = cc._decorator;

@ccclass
export default class item extends cc.Component {
    

    // onLoad () {}

    onLoad() {
        
    }
 
    start () {
       
    }

    createMachine(){
        
        cc.loader.loadRes( 'prefab/RankListItem', cc.Prefab, (prefab)=>
        {
            var node = cc.instantiate( prefab );
            // cc.find( 'Canvas' ).addChild( node );
            return node;
          
        }); 

    }
    
  

    // update (dt) {}
}
