// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class prefabMachine extends cc.Component {

    @property(cc.Label)
    num: cc.Label = null;

    @property(cc.Sprite)
    machine: cc.Sprite = null;
 

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    setMachineNum(num: string){
        this.num.string = num;
    }

    setMachineStatus(status: string){
        //確認機台是否有人
    }

    // update (dt) {}
}
