// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../Core/view/NotifyComponent";
import {core} from "../Core/Core";
import {NT} from "../constant/constant";


const {ccclass, property} = cc._decorator;

@ccclass
export default class zoomInOut extends cc.Component {




    @property(cc.Button)
    btn : cc.Button = null;

    private btnSize = 0;
   

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
        this.btnSize = this.btn.node.scale;
    }

    zoom_in_out(){
        cc.tween(this.btn.node)
        .to(0.1, {scale: this.btnSize+0.1})
        .to(0.1, {scale: this.btnSize})
        .start()
        
    }


    

  



 
}
