import {NT} from "../constant/constant";
import {core} from "../Core/Core";
import {NotifyComponent} from "../Core/view/NotifyComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Login extends NotifyComponent {
    
    start () {
        this.regist();
        core.getInstance().connectTo(core.TYPE.PK5GAME , 1);
    }

    sendInfo(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'AuthConnect', 
            objKey: 2,
            data: {
                // account: this.id.string,
                // pwd: this.pw.string,
                account: "test",
                pwd: "test",
                vendor: "10003"
            }
        });

    }

    notify(data) {
        NT.money = data.money;
        NT.uid = data.uid;
        NT.userName = data.name;
        cc.director.loadScene("Load");
    }

}
