import {Message} from "../lib/Msg/Message";
import {MsgToJson} from "../lib/Msg/MsgToJson";
import {MsgToByte} from "../lib/Msg/MsgToByte";
import {MsgProto} from "../lib/Msg/MsgProto";


//MsgProto.getInstance().init();
enum MSGTYPE{
    STRING =0 ,
    PROTOBUF =1,
    BYTE = 2
}

export class MessageAdopter  {
    
    public static MSGTYPE = MSGTYPE;

    public static getDecodeMsg(type: MSGTYPE , msg:any ) :Message {

        if(type== MSGTYPE.STRING ){

            return MsgToByte.getInstance();
        }
        else if( type ==MSGTYPE.BYTE ){
            
            return MsgToJson.getInstance();
        }
        else if(type == MSGTYPE.PROTOBUF ){
            if(!MsgProto.msgProto){
                MsgProto.getInstance().init();
            }
            return MsgProto.getInstance();
        }
    }

}
