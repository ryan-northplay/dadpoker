/**
 *
 * @author 
 *
 */
import {NETWORK } from "./../../constant/core_constant";
import {RootData} from "./../RootData";

export  class Subject {
    
    private observers: Observer[];
    
	public constructor() {
       
        this.observers = [];
        for(var k in NETWORK.STATE ){
            this.observers.push(null);
        }
	}
	
    public register( state ,  observer: Observer ) :void {
        this.observers[ state ] = null;
        this.observers[ state ]=  observer ;
        
	}
	
    public unregister( state ,  observer: Observer ):void{
        this.observers[ state ] = null;
        //this.observers.splice(state , 1 );
        
	}

    public notify(data: any ):void{
        
      
        
        if(this.observers[ data.key ]) {
            this.observers[ data.key ].notify(data);
        }
       
    }	
}

