
import {PHNotification} from '../model/Oberver/PHNotification';
import CmdReceiver from './CmdReceiver';
import {CmdInvoker} from './CmdInvoker';
import {Command} from "./Command";
import {DataModel} from "../model/DataModel";

import {CMD} from "./CMDSet";
import {MODEL} from "../model/ModelSet";

import {TYPE} from "../model/TypeSet";

import {NETWORK}   from "../constant/core_constant";

enum SERVERREF{
}



export  class Controller {
    //0: LoginServer , 1: GameServer , 2: ChatServer 
    private rv: CmdReceiver[];
    private invoker : CmdInvoker[];

    private static ctrl : Controller;
    private Connect :any={};
    private model:any;
    private closeFn : Function;

    private afterConnCallStart  =[];


    constructor(){
        this.rv = [];
        this.invoker =[];
        
    
        for(var a=0,len = NETWORK.Config.length ; a<len ;a++ ){
            // TYPE[a] = ConnectConfig[a].TYPE;
            TYPE[NETWORK.Config[a].TYPE] = a;
            SERVERREF[NETWORK.Config[a].TYPE ] =a;
        }
        
        this.Init();
        
    }

    public Init(){
        //start model connect 
        
    }

    //implement registView
    // regist  order by NETSTATE 
    public registView( state?:number , view?:any  ){
        console.log("regist state: ",state )
        PHNotification.getInstance().register( state  ,view );
       
    }

    public unregistView(state?:number,view?:any) {
        PHNotification.getInstance().unregister(state,view);
    }

    public CmdReceiver( type : number ) : CmdReceiver{

        return this.rv[type];
        
    }
    public registCmd( type : number , cmd: Command  ){

        this.invoker[type].storeCmd(cmd);
    }

    public startServerConnect( serverType: number , connectState : number , closeFn?: Function):boolean{
     
        // if( this.Connect[serverType] ){
        //     this.Connect[serverType].connect();
        //     //this.Connect[serverType] = null;
        //     return false;
        // }    
        
        var typeStr = NETWORK.Config[serverType].TYPE;
        
        
        this.Connect[serverType] = new DataModel( 
            NETWORK.Config[serverType].TYPE ,
            NETWORK.Config[serverType].HOST ,
            NETWORK.Config[serverType].PORT, connectState ,closeFn , [] ,NETWORK.Config[serverType].isSSL );

        
        this.Connect[serverType].connect();

        // this.rv[serverType] = new CmdReceiver( this.Connect[serverType] ) ;
        
        // this.invoker[serverType] = new CmdInvoker();

        // console.log("typeStr: "+typeStr);

        // for(var k in CMD[typeStr]){

        //     var a = CMD[typeStr][k].getInstance(this.rv[serverType]);
            
        //     this.invoker[serverType].storeCmd(a);

        // }
        // this.afterConnCallStart [serverType] = connectState;
        

        return true;
    }

    public close(serverType){
        this.Connect[serverType].close();
    }

    public send( serverType:TYPE , param:any){
        
        if(param.key){
            console.log("controller herer:=",serverType , param)
            console.log(this.Connect);
            this.Connect[serverType].sendToServer(param);
        }
        else{
            throw ("the data need key property");
        }
        
    }

    public block (serverType: TYPE , isBlock :boolean){
        this.Connect[serverType].block(isBlock);
    }
    

    
    public callCmd( type , state: number  , data: any ):void{


        this.invoker[type].exeCmd( state , data );
        try{
            
            //this.invoker[type].exeCmd( state , data );
        }
        catch(e){
            
            //egret.warn("type : error "+ type +" state : "+ state + " , data: ",data);
        }
        
    }

    public getCmdReceiver(serverType) {

        return this.rv[serverType];
    }

    public addCmd(serverType,cmd) {

        this.invoker[serverType].storeCmd(cmd);
    }

    public static getInstance():Controller {

        if( Controller.ctrl===null || Controller.ctrl===undefined ){
            
            Controller.ctrl = new Controller();
        }

        return Controller.ctrl;
    }

    
    public runTest(type : TYPE ){
        //ModuleLoader.getInstance(type);
    }

    public cmdLoader(serverType: number ){

        if(!this.rv[serverType] || this.rv[serverType] === null ){
            console.error("Server type: "+serverType + " Cmd rv is null");
            return ;
        }

    }

    public modelLoader(serverType: number ){
        if(!this.Connect[serverType] || this.Connect[serverType] === null ){
            console.error("Server type: "+serverType + " Connect is null");
            return ;
        }
    }


    public removeConn(serverType: string ){
        delete  this.Connect[ SERVERREF[ serverType] ];
        this.Connect[SERVERREF[ serverType] ] = null;
    }

    public startServerReconnect(serverType :number   ){
        
        if( this.Connect[serverType ] ){
          
            //this.Connect[serverType].close();
            //this.Connect[serverType].connect();
           this.Connect[serverType ] = null;
            //return ;
        }    
        
        if(!this.afterConnCallStart [serverType] ){
            //egret.warn("Reconnect : error , firest time connect  use the server connect  ");
            return ;
        }

        var typeStr = NETWORK.Config[serverType].TYPE;
        
        for(var k in MODEL[typeStr]){
         
            MODEL[typeStr][k].getInstance();
        }
        
                                                
        this.Connect[serverType] = new DataModel( NETWORK.Config[serverType].TYPE ,NETWORK.Config[serverType].HOST ,NETWORK.Config[serverType].PORT, this.afterConnCallStart [serverType], this.closeFn ,null );
        this.Connect[serverType].connect();
        this.rv[serverType] = new CmdReceiver( this.Connect[serverType] ) ;

        this.invoker[serverType] = new CmdInvoker();


        for(var k in CMD[typeStr]){

            var a = CMD[typeStr][k].getInstance(this.rv[serverType]);
            
            this.invoker[serverType].storeCmd(a);

        }
        return true;
        
        
    }
}