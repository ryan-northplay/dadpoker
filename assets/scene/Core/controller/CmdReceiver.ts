/**
 * CmdReceiver
 */
import {DataModel} from "../model/DataModel";

export default class CmdReceiver {
    
    private model : DataModel;

    constructor ( model :DataModel ){
        this.model = model;
    }
    
    doAction(state : number ,data: any , isLocal: boolean , header?:any){
        //call model sned 
       this.model.send( state, data, isLocal ,header);
    }
}